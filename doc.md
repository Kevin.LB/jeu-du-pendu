# JEU DU PENDU

''' Bash

Nom : LE BRETON Kévin Groupe :14B

Liste des tâches à effectuer :

□ Mettre en place le projet (projet VSCode + dépot git)

□ Coder la classe ControleurLettres

□ Coder la classe ControleurParametres

□ Coder la classe ControleurLancerPartie

□ Coder la classe ControleurNiveau

□ Coder la classe ControleurChronometre

□ Coder la classe ControleurInfos

□ Coder la classe Clavier

□ Coder le constructeur de Clavier

□ Coder la méthode desactiveTouches() de Clavier

□ Coder la classe Chronometre

□ Coder la classe Pendu

□ Afficher la page d’accueil

□ Afficher la page de jeu

□ La barre de progression doit indiquer l’avancement du jeu

□ Coder la méthode init() de Pendu

□ Coder la méthode titre() de Pendu

□ Coder la méthode modeAccueil() de Pendu

□ Coder la méthode modeJeu() de Pendu

□ Coder la méthode titre() de Pendu

□ Coder la méthode leChrono() de Pendu

□ Coder la méthode majAffichage() de Pendu

□ Coder la méthode getChrono() de Pendu

□ Coder la méthode popUpReglesDuJeu() de Pendu

□ Coder la méthode popUpMessageGagne() de Pendu

□ Coder la méthode popUpMessagePerdu() de Pendu

□ Coder la méthode modeParametres() de Pendu

□ Rendre tous les boutons fonctionnels

□ Rendre le chronomètre

□ Ajouter une infobulle sur les bouton "Home" , 
"Parametres" et "Info"

□ Griser le bouton "Home" sur la page d’acceuil

□ Griser le bouton "Parametres" sur la page de jeu

□ Refactoriser le code

□ Faire en sorte que les lettres déjà proposées par l’utilisateur soient désactivées

□ Gérer la fin de partie (gagné ou perdue)

□ Ajouter la possibilité de jouer à un autre jeu : le démineur

Remarque : je rappelle que "coder une méthode" inclut également "écrire sa documentation"




'''