import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.ButtonBar.ButtonData;

import java.util.List;

import javax.swing.GroupLayout;

import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;

/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;
    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;
    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;
    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;
    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    /**
     * le bouton Accueil / Maison
     */
    private Button boutonMaison;
    /**
     * le bouton Information / I
     */
    private Button bInfo;
    /**
     * le bouton Accueil / Maison
     */
    private Button bJouerButton;

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono
     * ...)
     */
    @Override
    public void init() {
        this.modelePendu = new MotMystere("mot", 3, 10, MotMystere.FACILE, 10);
        this.lesImages = new ArrayList<Image>();
        this.chargerImages("./img");
        this.boutonMaison = new Button();
        this.bJouerButton = new Button("");
        this.bJouerButton.setOnAction(new ControleurLancerPartie(this.modelePendu, this));

        ImageView homeimg = new ImageView(new Image("file:./img/home.png"));
        homeimg.setFitHeight(30);
        homeimg.setFitWidth(30);
        this.boutonMaison = new Button(null, homeimg);
        this.boutonMaison.setTooltip(new Tooltip("Home"));
        this.boutonMaison.setOnAction(new RetourAccueil(this.modelePendu, this));

        ImageView paramimg = new ImageView(new Image("file:./img/parametres.png"));
        paramimg.setFitHeight(30);
        paramimg.setFitWidth(30);
        this.boutonParametres = new Button(null, paramimg);
        this.boutonParametres.setTooltip(new Tooltip("Paramètres"));

        ImageView playimg = new ImageView(new Image("file:./img/info.png"));
        playimg.setFitHeight(30);
        playimg.setFitWidth(30);
        this.bInfo = new Button(null, playimg);
        this.bInfo.setTooltip(new Tooltip("Informations"));
        this.bInfo.setOnAction(new ControleurInfos(this));

        this.panelCentral = new BorderPane();

        this.chrono = new Chronometre();    
    }

    /**
     * @return le graphe de scène de la vue à partir de methodes précédantes
     */
    private Scene laScene() {
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.panelCentral);
        Scene scene = new Scene(fenetre, 600, 800);
        scene.setOnKeyPressed(new ControleurClavier(modelePendu, this));
        return scene;
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    private Pane titre() {
        BorderPane top = new BorderPane();
        top.setStyle("-fx-background-color: #E6E6FA;");
        Label LabelNom = new Label("Jeu du Pendu");
        LabelNom.setPadding(new Insets(10));
        LabelNom.setAlignment(Pos.CENTER);

        LabelNom.setFont(Font.font("Arial", 30));
        top.setLeft(LabelNom);

        HBox boutons = new HBox();
        boutons.getChildren().addAll(this.boutonMaison, this.boutonParametres, this.bInfo);
        boutons.setSpacing(10);
        boutons.setPadding(new Insets(10));
        top.setRight(boutons);

        return top;

    }

    /**
    * @return le panel du chronomètre
    */

    private TitledPane leChrono(){
    //A implementer
        TitledPane res = new TitledPane();
        res.setText("Chronomètre");
        res.setPadding(new Insets(10));
        chrono.setTime(0);
        chrono.start();
        res.setContent(chrono);
        res.collapsibleProperty().set(false);
        return res;
    }

    /**
    * @return la fenêtre de jeu avec le mot crypté, l'image, la barre
    * de progression et le clavier
    */

    private Pane fenetreJeu() {

        BorderPane res = new BorderPane();

        this.chrono.resetTime();

        this.motCrypte = new Text(this.modelePendu.getMotCrypte());
        this.motCrypte.setFont(Font.font("Arial", 30));
        this.motCrypte.setTextAlignment(TextAlignment.CENTER);

        this.dessin = new ImageView(this.lesImages.get(0));
        this.pg = new ProgressBar(0.0);
        this.clavier = new Clavier("ABCDEFGHIJKLMNOPQRSTUVWXYZ", new ControleurLettres(modelePendu, this));

        this.boutonMaison.setDisable(false);
        this.boutonParametres.setDisable(true);

        VBox Vboxgauche = new VBox();
        VBox Vboxdroite = new VBox();

        Vboxgauche.getChildren().addAll(this.motCrypte, this.dessin, this.pg, this.clavier);

        res.setLeft(Vboxgauche);

        TitledPane chrono = this.leChrono();

        if (this.modelePendu.getNiveau() == MotMystere.FACILE)
            this.leNiveau = new Text("Niveau Facile");

        else if (this.modelePendu.getNiveau() == MotMystere.MOYEN)
            this.leNiveau = new Text("Niveau Moyen");
        else if (this.modelePendu.getNiveau() == MotMystere.DIFFICILE)
            this.leNiveau = new Text("Niveau Difficile");
        else if (this.modelePendu.getNiveau() == MotMystere.EXPERT)
            this.leNiveau = new Text("Niveau Expert");
        this.leNiveau.setFont(Font.font("Arial",20));

        this.bJouerButton = new Button("Nouveau mot");
        this.bJouerButton.setOnAction(new ControleurLancerPartie(modelePendu, this));

        Vboxdroite.setAlignment(Pos.TOP_CENTER);
        Vboxdroite.setSpacing(20);
        Vboxdroite.setPadding(new Insets(10));;
        Vboxdroite.getChildren().addAll(this.leNiveau, chrono, this.bJouerButton);

        Vboxgauche.setAlignment(Pos.CENTER);
        Vboxgauche.setSpacing(10);
        Vboxgauche.setPadding(new Insets(10));

        res.setRight(Vboxdroite);

        System.out.println(this.modelePendu.getMotATrouver());

        return res;
    }

    /**
     * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de
     *         jeu
     */
    private Pane fenetreAccueil() {

        BorderPane bp = new BorderPane();
        VBox res = new VBox();
        VBox niveau = new VBox();

        this.boutonMaison.setDisable(true);
        this.boutonParametres.setDisable(false);

        this.bJouerButton = new Button("Lancer une partie");
        this.bJouerButton.setFont(Font.font("Arial", 15));
        this.bJouerButton.setPadding(new Insets(10));
        this.bJouerButton.setOnAction(new ControleurLancerPartie(modelePendu, this));

        TitledPane tp = new TitledPane();
        tp.setText("Niveau de difficulté");
        tp.setPadding(new Insets(10));

        RadioButton rb1 = new RadioButton("Facile");
        RadioButton rb2 = new RadioButton("Moyen");
        RadioButton rb3 = new RadioButton("Difficile");
        RadioButton rb4 = new RadioButton("Expert");

        rb1.setPadding(new Insets(5));
        rb2.setPadding(new Insets(5));
        rb3.setPadding(new Insets(5));
        rb4.setPadding(new Insets(5));

        rb1.setOnAction(new ControleurNiveau(modelePendu));
        rb2.setOnAction(new ControleurNiveau(modelePendu));
        rb3.setOnAction(new ControleurNiveau(modelePendu));
        rb4.setOnAction(new ControleurNiveau(modelePendu));

        if (modelePendu.getNiveau() == MotMystere.FACILE)
            rb1.setSelected(true);
        else if (modelePendu.getNiveau() == MotMystere.MOYEN)
            rb2.setSelected(true);
        else if (modelePendu.getNiveau() == MotMystere.DIFFICILE)
            rb3.setSelected(true);
        else if (modelePendu.getNiveau() == MotMystere.EXPERT)
            rb4.setSelected(true);

        ToggleGroup groupeBoutoons = new ToggleGroup();

        rb1.setToggleGroup(groupeBoutoons);
        rb2.setToggleGroup(groupeBoutoons);
        rb3.setToggleGroup(groupeBoutoons);
        rb4.setToggleGroup(groupeBoutoons);

        niveau.getChildren().addAll(rb1, rb2, rb3, rb4);

        tp.setContent(niveau);
        tp.collapsibleProperty().set(false);
        res.getChildren().addAll(this.bJouerButton, tp);

        res.setPadding(new Insets(10));

        bp.setCenter(res);
        this.panelCentral.setCenter(bp);

        return bp;

    }

    /**
     * charge les images à afficher en fonction des erreurs
     * 
     * @param repertoire répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire) {
        for (int i = 0; i < this.modelePendu.getNbErreursMax() + 1; i++) {
            File file = new File(repertoire + "/pendu" + i + ".png");
            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }

    public void modeAccueil() {

        this.panelCentral.setCenter(this.fenetreAccueil());

    }

    public void modeJeu() {

        this.panelCentral.setCenter(this.fenetreJeu());
    }

    public void modeParametres() {
        // A implémenter
    }

    /** lance une partie */
    public void lancePartie() {
        this.modeJeu();
    }

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage() {
    
        this.motCrypte.setText(this.modelePendu.getMotCrypte());
        this.pg.setProgress(0.1 * this.modelePendu.getNbEssais());
        if (!this.chrono.getEnCours()){
            this.chrono.stop();
            this.modeAccueil();
            this.popUpMessagePerdu().showAndWait();
            this.modelePendu.setMotATrouver();
            System.out.println("Perdu");
        }
        try {
            this.dessin.setImage(this.lesImages.get(this.modelePendu.getNbErreursMax()- this.modelePendu.getNbErreursRestants()));
        } catch (Exception e) {
            this.dessin.setImage(this.lesImages.get(this.lesImages.size() - 1));
            System.out.println("Erreur : "+"plus d'images à afficher");
        }

    }

    /**
     * accesseur du chronomètre (pour les controleur du jeu)
     * 
     * @return le chronomètre du jeu
     */
    public Chronometre getChrono() {
        return this.chrono; 
    }

    public Alert popUpLancerPartie() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "La partie va se lancer"+"\n"+"Voulez-vous continuer ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }

    public Alert popUpPartieEnCours() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Une partie est en cours"+"\n"+"Voulez-vous l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }

    public Alert popUpReglesDuJeu() {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Règles du jeu");
        alert.setHeaderText("Règles du jeu");
        alert.setContentText("Le jeu du Pendu consiste à deviner un mot secret en proposant des lettres.\n"
                + "Le nombre d'essais est limité en fonction du niveau de difficulté choisi.\n"
                + "Si vous parvenez à deviner le mot avant d'épuiser tous les essais, vous gagnez la partie.\n"
                + "Sinon, vous perdez la partie.\n"
                + "Le mot secret est représenté par des tirets, chaque tiret représentant une lettre inconnue.\n"
                + "Vous pouvez proposer une lettre en cliquant sur les lettres disponibles.\n"
                + "Si la lettre proposée est présente dans le mot secret, elle sera révélée à sa position correspondante.\n"
                + "Si la lettre proposée n'est pas dans le mot secret, vous perdrez un essai.\n"
                + "Le jeu se termine lorsque vous avez trouvé le mot secret ou que vous avez épuisé tous les essais.\n"
                + "Bonne chance et amusez-vous bien !");

        return alert;
    }

    public Alert popUpMessageGagne() {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Jeu du Pendu");
        alert.setHeaderText("Bravo ! Vous avez gagné :)");
        alert.setContentText("Votre nombres de coups est de " + this.modelePendu.getNbEssais());
        return alert;
    }

    public Alert popUpMessagePerdu() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Jeu du Pendu");
        alert.setHeaderText("Dommage, vous avez perdu :(");
        alert.setContentText("Le mot à trouver était : " + this.modelePendu.getMotATrouver());
        return alert;
    }


    public Clavier getClavier(){
        return this.clavier;
    }

    /**
     * créer le graphe de scène et lance le jeu
     * 
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("Le jeu du pendu par LE BRETON KEVIN");
        stage.setScene(this.laScene());
        this.modeAccueil();
        stage.show();
    
    }

    /**
     * Programme principal
     * 
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }
}
