import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurNouveauMot implements EventHandler<ActionEvent> {

    /**
     * vue du jeu
     */
    private Pendu vuePendu;
    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**

    /**
     * @param modelePendu modèle du jeu
     * @param vuePendu vue du jeu
     */
    ControleurNouveauMot(MotMystere modelePendu, Pendu vuePendu){
        this.vuePendu = vuePendu;
        this.modelePendu = modelePendu;
    }

    
    /**
     * Actions à effectuer lors du clic sur le bouton Nouveau Mot
     * @param actionEvent l'événement
     */

    @Override
    public void handle(ActionEvent actionEvent) {
        
        this.vuePendu.modeJeu();
        this.modelePendu.setMotATrouver();
        
    }
}
