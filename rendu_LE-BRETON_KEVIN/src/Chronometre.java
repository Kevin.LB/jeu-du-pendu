import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.text.Text;
import javafx.util.Duration;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;


/**
 * Permet de gérer un Text associé à une Timeline pour afficher un temps écoulé
 */
public class Chronometre extends Text{
    /**
     * timeline qui va gérer le temps
     */
    private Timeline timeline;
    /**
     * la fenêtre de temps
     */
    private KeyFrame keyFrame;
    /**
     * le contrôleur associé au chronomètre
     */
    private ControleurChronometre actionTemps;
    /**
     * true si le chronomètre n'est pas à 0
     */
    private boolean enCours;

    /**
     * temps initial en milliseconde
     */
    private long initialTimeMillis;

    /**
     * Constructeur permettant de créer le chronomètre
     * avec un label initialisé à "0:0:0"
     * Ce constructeur créer la Timeline, la KeyFrame et le contrôleur
     */
    public Chronometre(){

        this.actionTemps = new ControleurChronometre(this);
        this.timeline = new Timeline();
        this.keyFrame = new KeyFrame(Duration.seconds(1), this.actionTemps);
        this.timeline.setCycleCount(Animation.INDEFINITE);
        this.timeline.getKeyFrames().add(this.keyFrame);
        this.setText("5:00");
        this.setFont(new Font(20));
        this.setTextAlignment(TextAlignment.CENTER);
        this.initialTimeMillis = 5 * 60 * 1000;
        this.enCours = true;

    }

    /**
     * Permet au controleur de mettre à jour le text
     * la durée est affichée sous la forme m:s
     * @param tempsMillisec la durée depuis à afficher
     */
    public void setTime(long tempsMillisec){
        long tempsRestant = initialTimeMillis - tempsMillisec;
        if (tempsRestant <= 0) {
            this.setText("0:00");
            this.stop();
            this.enCours = false;
        } else {
            long seconds = tempsRestant / 1000;
            long minutes = seconds / 60;
            seconds = seconds % 60;
            this.setText(String.format("%d:%02d", minutes, seconds));
        }
    }

    /**
     * Permet de démarrer le chronomètre
     */
    public void start(){
        
        this.timeline.play();
    }

    /**
     * Permet d'arrêter le chronomètre
     */
    public void stop(){
            
        this.timeline.stop();
    }

    /**
     * Permet de connaitre la valeur de l'attribut enCours
     * @return boolean enCours
     */
    public boolean getEnCours(){
        
        return this.enCours;
    }
    /**
     * Permet de remettre le chronomètre à 0
     */
    public void resetTime(){
        
        this.timeline.stop();
        this.timeline.getKeyFrames().remove(this.keyFrame);
        this.timeline.getKeyFrames().add(this.keyFrame);
        this.actionTemps.reset();
        this.setText("5:00");
        this.enCours = true;

    }
}
