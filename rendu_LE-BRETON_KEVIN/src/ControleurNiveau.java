import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;

/**
 * Controleur des radio boutons gérant le niveau
 */
public class ControleurNiveau implements EventHandler<ActionEvent> {

    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;

    /**
     * @param modelePendu modèle du jeu
     */
    public ControleurNiveau(MotMystere modelePendu) {

        this.modelePendu = modelePendu;

    }

    /**
     * gère le changement de niveau
     * 
     * @param actionEvent
     */
    @Override
    public void handle(ActionEvent actionEvent) {

        RadioButton b = (RadioButton) actionEvent.getTarget();

        if (b.getText().equals("Facile")) {
            this.modelePendu.setNiveau(0);
        } else if (b.getText().equals("Moyen")) {
            this.modelePendu.setNiveau(1);
        } else if (b.getText().equals("Difficile")) {
            this.modelePendu.setNiveau(2);
        } else if (b.getText().equals("Expert")) {
            this.modelePendu.setNiveau(3);
        }
        String nomDuRadiobouton = b.getText();
        System.out.println(nomDuRadiobouton);
    }
}
