import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.event.EventHandler;

public class ControleurClavier implements EventHandler<KeyEvent>{
    
    
  /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**
     * vue du jeu
     */
    private Pendu vuePendu;

    /**
     * @param modelePendu modèle du jeu
     * @param vuePendu vue du jeu
     */
    ControleurClavier(MotMystere modelePendu, Pendu vuePendu){
        this.modelePendu = modelePendu;
        this.vuePendu = vuePendu;
    }
    
    
    public void handle(KeyEvent e){       
        

        try {
            char lettre = e.getText().toUpperCase().charAt(0);
        this.vuePendu.getClavier().desactiveTouches(this.modelePendu.getLettresEssayees());


        if (this.modelePendu.essaiLettre(lettre)>0){
            this.vuePendu.majAffichage();
            if (this.modelePendu.gagne()){
                System.out.println("Gagne");
                this.vuePendu.getChrono().stop();
                this.vuePendu.popUpMessageGagne().showAndWait();
                this.vuePendu.modeAccueil();
                this.modelePendu.setMotATrouver();
                
            }
            
        }
        else if (this.modelePendu.essaiLettre(lettre)==0){
            this.vuePendu.majAffichage();
            if (this.modelePendu.perdu()){
                System.out.println("Perdu");
                this.vuePendu.getChrono().stop();
                this.vuePendu.modeAccueil();
                this.vuePendu.popUpMessagePerdu().showAndWait();
                this.modelePendu.setMotATrouver();
            }
            
        }
            
        } catch (Exception exception) {
            System.out.println("Touche non valide");
        }
        

        
    }
}
